package ru.t1.vlvov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.model.ICommand;
import ru.t1.vlvov.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public final class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    private final String ARGUMENT = "-arg";

    @NotNull
    private final String DESCRIPTION = "Show arguments.";

    @NotNull
    private final String NAME = "arguments";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = commandService.getTerminalCommands();
        for (@NotNull final ICommand command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
