package ru.t1.vlvov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.TaskClearRequest;

@Component
public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    private final String NAME = "task-clear";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @Nullable final TaskClearRequest request = new TaskClearRequest(getToken());
        taskEndpoint.clearTask(request);
    }

}