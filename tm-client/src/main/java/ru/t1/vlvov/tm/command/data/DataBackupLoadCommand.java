package ru.t1.vlvov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.DataBackupLoadRequest;

@Component
public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load backup data from base 64 file.";

    @NotNull
    public static final String NAME = "data-load-backup";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull DataBackupLoadRequest request = new DataBackupLoadRequest(getToken());
        domainEndpoint.loadDataBackup(request);
    }

}
