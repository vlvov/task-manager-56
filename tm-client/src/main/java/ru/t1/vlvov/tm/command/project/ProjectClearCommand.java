package ru.t1.vlvov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.ProjectClearRequest;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Remove all projects.";

    @NotNull
    private final String NAME = "project-clear";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        @Nullable final ProjectClearRequest request = new ProjectClearRequest(getToken());
        projectEndpoint.clearProject(request);
    }

}
