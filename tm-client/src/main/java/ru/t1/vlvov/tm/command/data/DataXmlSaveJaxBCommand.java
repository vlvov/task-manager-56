package ru.t1.vlvov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.DataXmlSaveJaxBRequest;

@Component
public final class DataXmlSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save data to xml file.";

    @NotNull
    private final String NAME = "data-save-xml-jaxb";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        @NotNull DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest(getToken());
        domainEndpoint.saveDataXmlJaxB(request);
    }

}
