package ru.t1.vlvov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.DataBase64SaveRequest;

@Component
public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save data to base 64 file.";

    @NotNull
    private final String NAME = "data-save-text";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE BASE64]");
        @NotNull DataBase64SaveRequest request = new DataBase64SaveRequest(getToken());
        domainEndpoint.saveDataBase64(request);
    }

}
