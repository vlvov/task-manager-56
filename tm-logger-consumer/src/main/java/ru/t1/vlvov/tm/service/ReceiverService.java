package ru.t1.vlvov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.IReceiverService;
import ru.t1.vlvov.tm.listener.EventListener;

import javax.jms.*;

@Service
public class ReceiverService implements IReceiverService {

    @NotNull private static final String QUEUE = "LOGGER";

    @NotNull
    @Autowired
    private ConnectionFactory factory;

    @NotNull
    @Autowired
    private EventListener eventListener;

    @SneakyThrows
    public void receive() {
        @NotNull final Connection connection = factory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(eventListener);
    }

}
