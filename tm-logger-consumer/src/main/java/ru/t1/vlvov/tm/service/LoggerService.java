package ru.t1.vlvov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.ILoggerService;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Service
public class LoggerService implements ILoggerService {

    @SneakyThrows
    @Override
    public void log(@NotNull final String fileName, @NotNull final String text) {
        byte data[] = text.getBytes(StandardCharsets.UTF_8);
        Path file = Paths.get(fileName);
        if (!Files.exists(file)) Files.createFile(file);
        Files.write(file, data, StandardOpenOption.APPEND);
    }

}
