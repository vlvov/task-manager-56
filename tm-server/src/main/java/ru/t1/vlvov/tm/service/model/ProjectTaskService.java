package ru.t1.vlvov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.model.IProjectService;
import ru.t1.vlvov.tm.api.service.model.IProjectTaskService;
import ru.t1.vlvov.tm.api.service.model.ITaskService;
import ru.t1.vlvov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.vlvov.tm.exception.entity.TaskNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.model.Task;

import java.util.List;

@Service
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    public void bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(project);
        taskService.update(task);
    }

    @Override
    public void removeProjectById(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        List<Task> tasks = taskService.findAllByProjectId(projectId);
        if (tasks != null)
            for (final Task task : tasks) {
                taskService.remove(task);
            }
        projectService.remove(project);
    }

    @Override
    public void unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        if (!projectService.existsById(userId, projectId)) throw new ProjectNotFoundException();
        Task task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final String taskProjectId = task.getProject().getId();
        if (taskProjectId == null || taskProjectId.isEmpty() || !taskProjectId.equals(projectId))
            throw new TaskNotFoundException();
        task.setProject(null);
        taskService.update(task);
    }

}
