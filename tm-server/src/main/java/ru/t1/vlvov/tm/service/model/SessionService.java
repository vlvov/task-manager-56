package ru.t1.vlvov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.repository.model.ISessionRepository;
import ru.t1.vlvov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.model.ISessionService;
import ru.t1.vlvov.tm.model.Session;
import ru.t1.vlvov.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

@Service
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @Autowired
    private @NotNull ISessionRepository sessionRepository;

    @Override
    @NotNull
    protected ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }
}
