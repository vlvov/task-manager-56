package ru.t1.vlvov.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.vlvov.tm.api.service.dto.IProjectDtoService;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import ru.t1.vlvov.tm.exception.field.NameEmptyException;
import ru.t1.vlvov.tm.repository.dto.ProjectDtoRepository;

import javax.persistence.EntityManager;

@Service
@Getter
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDTO, IProjectDtoRepository> implements IProjectDtoService {

    @NotNull
    protected IProjectDtoRepository getRepository() {
        return context.getBean(ProjectDtoRepository.class);
    }

    @Override
    @NotNull
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO model = new ProjectDTO();
        model.setName(name);
        model.setUserId(userId);
        @NotNull final IProjectDtoRepository projectRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = projectRepositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepositoryDTO.add(userId, model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO model = new ProjectDTO();
        model.setName(name);
        model.setUserId(userId);
        model.setDescription(description);
        @NotNull final IProjectDtoRepository projectRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = projectRepositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepositoryDTO.add(userId, model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        @NotNull final IProjectDtoRepository projectRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = projectRepositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepositoryDTO.update(userId, model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        @NotNull final IProjectDtoRepository projectRepositoryDTO = getRepository();
        @NotNull final EntityManager entityManager = projectRepositoryDTO.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepositoryDTO.update(model);
            entityManager.getTransaction().commit();
            return model;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
