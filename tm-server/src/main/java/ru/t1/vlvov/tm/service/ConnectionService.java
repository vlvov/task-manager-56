package ru.t1.vlvov.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.vlvov.tm.api.service.IConnectionService;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.dto.model.SessionDTO;
import ru.t1.vlvov.tm.dto.model.TaskDTO;
import ru.t1.vlvov.tm.dto.model.UserDTO;
import ru.t1.vlvov.tm.listener.EventListener;
import ru.t1.vlvov.tm.listener.JMSLoggerProducer;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.model.Session;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Service
public final class ConnectionService implements IConnectionService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private EventListener eventListener;

    @NotNull
    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @NotNull
    @Override
    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getDatabaseUser());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2ddl());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseSecondLvlCash());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDatabaseFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getDatabaseUseQueryCash());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDatabaseUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDatabaseRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDatabaseConfigFilePath());
        settings.put(Environment.FORMAT_SQL, propertyService.getDatabaseFormatSql());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        final EntityManagerFactory entityManagerFactory = metadata.getSessionFactoryBuilder().build();
        final SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        final EventListenerRegistry registryListener = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        registryListener.getEventListenerGroup(EventType.POST_INSERT).appendListener(eventListener);
        registryListener.getEventListenerGroup(EventType.POST_DELETE).appendListener(eventListener);
        registryListener.getEventListenerGroup(EventType.POST_UPDATE).appendListener(eventListener);
        return entityManagerFactory;
    }

    @Override
    @NotNull
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
