package ru.t1.vlvov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.vlvov.tm.dto.model.SessionDTO;
import ru.t1.vlvov.tm.enumerated.Sort;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Scope("prototype")
public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> implements ISessionDtoRepository {

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM SessionDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    @Nullable
    public List<SessionDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m";
        return entityManager.createQuery(jpql, SessionDTO.class).getResultList();
    }

    @Override
    @Nullable
    public SessionDTO findOneById(@NotNull final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Override
    public void removeById(@NotNull final String id) {
        @Nullable final SessionDTO model = findOneById(id);
        if (model != null) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return findOneById(id) != null;
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM SessionDTO WHERE userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    @Nullable
    public List<SessionDTO> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public SessionDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE userId = :userId AND id = :id";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final SessionDTO model = findOneById(userId, id);
        if (model != null) entityManager.remove(model);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<SessionDTO> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<SessionDTO> criteriaQuery = criteriaBuilder.createQuery(SessionDTO.class);
        Root<SessionDTO> session = criteriaQuery.from(SessionDTO.class);
        criteriaQuery.select(session)
                .where(criteriaBuilder.equal(session.get("userId"), userId))
                .orderBy(criteriaBuilder.asc(session.get(Sort.getOrderByField(sort))));
        TypedQuery<SessionDTO> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

}
