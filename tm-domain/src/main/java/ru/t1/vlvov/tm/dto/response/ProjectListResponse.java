package ru.t1.vlvov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListResponse extends AbstractResponse {

    private List<ProjectDTO> projects;

    public ProjectListResponse(List<ProjectDTO> projects) {
        this.projects = projects;
    }

}
